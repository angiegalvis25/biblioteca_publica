import React, { useEffect, useState } from "react";
import { Row, Col, Button } from "react-bootstrap";
import Modal from 'react-bootstrap/Modal';

function ModalInfo(props) {

    const [show, setShow] = useState(false);
    const handleShow = () => setShow(true);


    const handleClose = () => {
        setShow(false);
    }

    return (
        <>
            <Button
                variant="primary"
                onClick={handleShow}
                className="btModalServicio"
            >
                Ver
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
            >
                <Modal.Body>

                    <Row >
                        <Col className="col-12">
                            <p className="txtModal my-3">
                                id:

                                {props.data.id}
                            </p>
                            
                            <p className="txtModal my-3">
                                titulo:
                                {props.data.title}
                            </p>
                            <p className="txtModal my-3">
                                descripcion:
                                {props.data.body}
                            </p>
                        </Col>
                    </Row>
                    <Row className="justify-content-center">
                        <Col className="col-auto p-1">
                            <Button
                                className="btVerde"
                                id="Volver"
                                onClick={handleClose}
                            >
                                Aceptar
                            </Button>
                        </Col>
                    </Row>
                </Modal.Body>
            </Modal>
        </>
    );
}

export default ModalInfo;
