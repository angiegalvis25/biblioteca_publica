import React, { useEffect, useState } from "react";
import { Row, Col, Button } from "react-bootstrap";
import Modal from 'react-bootstrap/Modal';



function ModalCrear(props) {

    const [show, setShow] = useState(false);
    const handleShow = () => setShow(true);
    const url ="https://jsonplaceholder.typicode.com/posts/"
    const [id,setId]=useState();
    const [title,setTitle]=useState();
    const [body,setBody]=useState();
    const handleRedirect = (route) => { History.push(route) }
    const onSubmit = (e) => {
        e.preventDefault();
        let json = {
            "id": id,
            "title": title,
            "body": body,
            
        }
        console.log('el json', json, 'que paso', url);
        fetch(url, {
            method: "POST",
            body: JSON.stringify(json),
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                
            },
        }).then((res) => res.json())
            .then((res1) => {
                console.log('res1', res1);
                if (res1.success) {
                   <h1>creado</h1>
                    setId("");
                    setBody("");
                    setTitle("")
                    handleRedirect('../View/App.js')
                }
            })

    }

    const handleClose = () => {
        setShow(false);
    }

    return (
        <>

            <Button
                variant="primary"
                onClick={handleShow}
                className="btDescargar"
            >
                Añadir libro
            </Button>



            <Modal
                show={show}
                onHide={handleClose}
            >
                <Modal.Body>
                
                    <Row  >
                        <Col className="col-12">
                            <Col className="col-12">
                                <label for="name">id usuario</label>

                                <select
                                                    as="select"
                                                    name="opciones"
                                                    value={id}
                                                    onChange={(e) => setId(e.target.value)}
                                                    required
                                                        className='form-select'
                                                >   
                                                    <option value="">selecciona</option>
                                                    <option value={1}>1</option>
                                                    <option value={2}>2</option>
                                                    <option value={3}>3</option>
                                                    <option value={4}>4</option>
                                                    <option value={5}>5</option>
                                                    
                                                </select>
                            </Col>
                            <Col className="col-12">
                                <label for="name">Title </label>

                                <input value={title} onChange={(e) => setTitle(e.target.value)}type="text" 
                                size={50} required/>
                            </Col>
                            <Col className="col-12">
                                <label for="name">Body </label>

                                <textarea value={body} onChange={(e) => setBody(e.target.value)} type="text" 
                                  cols="50" rows="5" required
                                  />
                            </Col>
                        </Col>
                    </Row>
                    <Row className="justify-content-center">
                        <Col className="col-auto p-1">
                            <Button
                                className="btDescargar"
                                id="Volver"
                                onClick={handleClose}
                            >
                                Cancelar
                            </Button>
                        </Col>
                        <Col className="col-auto p-1">
                            <Button
                                type="submit"
                                className="btDescargar"
                             onClick={onSubmit}
                            >
                                Guardar
                            </Button>
                        </Col>
                    </Row>
                </Modal.Body>
            </Modal>
        </>
    );
}

export default ModalCrear;