import React from 'react';
import "./Pagination.css";

const Pagination = ({ currentPage, postsPerPage, totalPosts, paginate }) => {
    const pageNumbers = [];
    const Lower = currentPage - 1;
    const Higher = currentPage + 1;

    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
        pageNumbers.push(i);
    }

    return (
        <div >
            <ul className='pagination'>
                <li className='page-item'>
                    <a onClick={() => paginate(1)} className='page-link'>{'<<'}</a>
                    <a onClick={() => paginate(Lower <= 0 ? 1 : Lower)} className='page-link'>{'<'}</a>
                    {pageNumbers.filter(number => number >= Lower && number <= Higher)
                        .map(number => (
                            <a key={number} onClick={() => paginate(number)} className={number == currentPage ? "active" : 'page-link'}>{number} </a>
                        ))}
                    <a onClick={() => paginate(Higher >= pageNumbers.length ? pageNumbers.length : Higher)} className='page-link'>{'>'}</a>
                    <a onClick={() => paginate(pageNumbers.length)} className='page-link'>{'>>'}</a>
                </li>
            </ul>
        </div>

    );
};

export default Pagination;