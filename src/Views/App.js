import { useEffect, useState } from 'react';
import { Row, Col, Container, Form, Button, Table } from "react-bootstrap";
import ModalCrear from '../Components/Modales/ModalCrear';
import ModalInfo from '../Components/Modales/ModalInfo';
import Pagination from '../Components/Pagination';
import './../App.css';
import './books.css'




const url = "https://jsonplaceholder.typicode.com/posts/"

function App() {
  const [data, setdata] = useState([])
  const [busqueda,setBusqueda] =useState("")
  const [results, setResults] = useState([])


  useEffect(() => {
    fetch(url, {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => response.json())
      .then(json => {
        setdata(json)
       
       
      })
      .catch(err => console.log('Solicitud fallida', err));
      
  }, [])
 

  //Paginador
  const [numero,setnumero] =useState("")
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage,setPostperpage] = useState(10);
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = data.slice(indexOfFirstPost, indexOfLastPost);
  const paginate = pageNumber => setCurrentPage(pageNumber);

  //buscador
  const buscador=(e)=>{
    setBusqueda(e.target.value,"esta es la busqueda")
    console.log(e.target.value)
    console.log(busqueda,"esta es la busqueda")
  }
  
//filtrado
const resultado = data.filter ((libro) => {

  if (libro.title.toUpperCase( ).includes (busqueda.toUpperCase( ))) {
  
  return true;

  }
  
  return false;
  
  });

  return (
    <div className="App">
      <Container fluid className="contGeneralHistorico">
        <Row className="justify-content-center align-items-center contCardHistorico">
          <Col className="col-11 contTable">
            <Row>
              <Col className="col-12 mb-4">
                <p className="titHistorico">
                  Listado de libros
                </p>
              </Col>
            </Row>
            <Col className="col-12">
              <label for="name">buscar:     </label>
              <input onChange={buscador}
              value={busqueda} type="text" size={50} />
            </Col>
                            
                     
            <Row>
              <Col>
                <Table responsive striped bordered hover size="sm" className="text-center">
                  <thead>
                    <tr className="tableHeadAdmin">
                      <th>
                        <p className="titTableHistorico">
                          Id
                        </p>
                      </th>
                      <th>
                        <p className="titTableHistorico">
                          Title
                        </p>
                      </th>
                      <th>
                        <p className="titTableHistorico">
                          Body
                        </p>
                      </th>
                      <th>
                        <p className="titTableHistorico">
                          Action
                        </p>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      data &&
                      currentPosts.map((e, i) => {
                        return (
                          <tr key={i} style={{ verticalAlign: "middle" }}>
                            <td>
                              <p className="txtTableHistorico">
                                {e.id}
                              </p>
                            </td>
                            <td>
                              <p className="txtTableHistorico">
                                {e.title}
                              </p>
                            </td>
                            <td>
                              <p className="txtTableHistorico">
                                {e.body.substring(0, 9)}
                              </p>
                            </td>
                            <td>
                              <p className="txtTableHistorico">
                                <ModalInfo data={e}>
                                </ModalInfo>
                              </p>
                            </td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </Table>
              </Col>
            </Row>
            <Row className="justify-content-center">
              <Col className="col-auto">
                <Pagination
                  currentPage={currentPage}
                  postsPerPage={postsPerPage}
                  totalPosts={data.length}
                  paginate={paginate}
                />
                  <select
                      as="select"
                      name="opciones"
                      onChange={(e) => {setPostperpage(e.target.value)}}
                      className='form-select'
                      >   
                      <option value="10">10</option>
                      <option value="20">20</option>
                      <option value="30">30</option>
                                                   
                                                    
              </select>
              </Col>
            </Row>
            <Row className="justify-content-center">
              <Col className="col-6 col-md-4 col-lg-2 text-center">

                <br/>

                  <ModalCrear>
                  </ModalCrear>

              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
